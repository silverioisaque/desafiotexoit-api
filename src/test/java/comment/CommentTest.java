package comment;

import base.BaseAPI;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;

public class CommentTest extends BaseAPI  {

    @Test
    @DisplayName("Pesquisar comentário pelo name")
    public void pesquisarComentarioNome(){

        given().
        when().
            get("users").
        then().
        statusCode(HttpStatus.SC_OK);
    }

}
