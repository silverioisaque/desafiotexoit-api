package user;

import base.BaseAPI;
import io.restassured.http.ContentType;
import model.User;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;


public class UserTest extends BaseAPI {

    @Test
    @DisplayName("Criar um novo Usuário")
    public void criarNovoUser(){
        User user = factoryUser.criarNovoUsuario();

        given().
            contentType(ContentType.JSON).
            body(user).
        when().
            post("users/").
        then().
            statusCode(HttpStatus.SC_CREATED).
            body("id", equalTo(user.getId()));
    }

    @Test
    @DisplayName("Alterar dados de um Usuário")
    public void AlterarUser(){
        User user = factoryUser.alterarDadosUsuario();

        given().
            contentType(ContentType.JSON).
            body(user).
        when().
            put("users/5").
        then().
            statusCode(HttpStatus.SC_OK).
            body(
                "id", equalTo(user.getId()),
                "email", equalTo(user.getEmail()),
                "lat", equalTo(user.getLat()),
                "lng", equalTo(user.getLng())
            );
    }
}
