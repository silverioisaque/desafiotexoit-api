package data;

import com.github.javafaker.Faker;
import model.User;

public class FactoryUser {

    private final Faker faker;

    public FactoryUser() {
        faker = new Faker();
    }

    public User criarNovoUsuario() {

        return new User().
            id(11).
            name(faker.name().fullName()).
            email(faker.internet().emailAddress()).
            address(faker.address().fullAddress()).
            phone(faker.phoneNumber().cellPhone()).
            webSite(faker.internet().domainName()).
            company(faker.company().name());
    }

    public User alterarDadosUsuario(){
        return new User().
            id(5).
            email(faker.internet().emailAddress()).
            lat(faker.address().latitude()).
            lng(faker.address().longitude());
    }
}
