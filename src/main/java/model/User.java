package model;

import lombok.Data;


@Data
public class User {
    private int id;
    private String name;
    private String email;
    private String address;
    private String phone;
    private String webSite;
    private String company;
    private String lat;
    private String lng;


    public User(int id, String name, String email, String address, String phone, String webSite, String company, String lat, String lng) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.address = address;
        this.phone = phone;
        this.webSite = webSite;
        this.company = company;
        this.lat = lat;
        this.lng = lng;
    }

    public User() {

    }

    public User id(int id) {
        this.id = id;
        return this;
    }

    public User name(String name) {
        this.name = name;
        return this;
    }

    public User email(String email) {
        this.email = email;
        return this;
    }
    public User address(String address) {
        this.address = address;
        return this;
    }
    public User phone(String phone) {
        this.phone = phone;
        return this;
    }

    public User webSite(String webSite) {
        this.webSite = webSite;
        return this;
    }

    public User company(String company) {
        this.company = company;
        return this;
    }

    public User lat (String lat){
        this.lat = lat;
        return this;
    }

    public User lng(String lng) {
        this.lng = lng;
        return this;
    }
}
