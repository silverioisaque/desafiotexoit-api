## 🔖&nbsp; Sobre

Desafio técnico de testes automatizados (API) do TexoIT

---

## 🚀 Tecnologias utilizadas

O projeto foi desenvolvido utilizando as seguintes tecnologias

- [Java](https://www.java.com/pt-BR/)
- [JUnit](https://junit.org/junit5/)
- [IntelliJ](https://www.jetbrains.com/pt-br/idea/)
- [AllureReport](https://github.com/allure-framework)
---
## 💻 Padrões de projeto

Base Test

Test Data Factory para gerar os dados fake

Request and Response Specification

---

Requisitos de software e execução

- Java 8 + JDK 14 deve estar instalado.
- Maven instalado e configurado no path da aplicação.


- Clonar o projeto para sua máquina git clone https://gitlab.com/silverioisaque/desafiotexoit-api.git
- Abrir na sua IDE preferencial
- mvn clean test
---
3 CTS  foram automatizados
![img.jpg](Screenshot_1.jpg)


![img_3.jpg](Screenshot_3.jpg)
